# `chainmaker-x-web3` the way to the Web3

## 如何开始
### 1. 部署长安链区块链网络
- 1.1 配置节点
```shell
启用web3功能需要关闭节点rpc对查询类tx的验签，具体步骤如下：
cd chainmaker-go/test/chain4/config
修改node1-node4下所有的chainmaker.yml中的rpc.disable_verify_query_signature为true
```
- 1.2 编译、启动、初始化
```shell
cd chainmaker-go/test/chain4
./build.sh
./start.sh
修改 test.sh 将 "Mint ETHToken 10亿" 下 account 的值改为 0x77F6797905382A4450aC9fe0292d85D5B22e4Fd1
./test.sh
```
通过`ps aux|grep chainmaker`查看节点进程已在运行中。

恭喜！此刻我们就已经在本地，成功启动了一个4节点的兼容以太坊web3的长安链网络了。
### 2. 部署 chainmaker-x-web3
- 2.1 配置
```shell
将 chainmaker-go/test/chain4/config/node1 文件夹拷贝覆盖到 chainmaker-x-web3/cmd/jrpcserver/node1
```
- 2.2 启动
```shell
cd chainmaker-x-web3/cmd/jrpcserver
go run main.go -c ./config.yml
```
恭喜！此刻我们就已经在本地，成功启动了一个 chainmaker-x-web3 网关了。
### 3. 测试
- metamask测试转账
```
钱包地址：0x77F6797905382A4450aC9fe0292d85D5B22e4Fd1
私钥：f71d17a0eeaaa4b3ffe00992cac17a66e3799ccbf0ffd913b9c3a3334ff7dc65

RPC URL：http://127.0.0.1:80
链ID：12301
货币符号：CM

将上述账号0x77F6797905382A4450aC9fe0292d85D5B22e4Fd1倒入metamask
在metamask中新增上述网络配置

这时候就可以看到CM余额了，并且可以给任何地址转账CM了！
```
- remix测试ERC20
1. 打开 https://remix.ethereum.org/
2. 将 chainmaker-x-web3/docs/erc20.sol 源码拷贝到 remix ide 中
3. 编译 erc20.sol 如下图

![remix compile](./docs/remix1.png)

4. 部署 USDT 如下图，选择 Injected Provider - MetaMask，选择 USDT 合约，   
   Deploy栏输入初始化参数 分别是token名字，token缩写，token的小数点位数，       
   token最大发行量，token部署时给sender的金额。    
   比如：Tether USD,USDT,6,1000000000000000000,100000000000000      
   最后点击Deploy

![remix deploy](./docs/remix2.png)

5. 部署成功后可以看到刚部署的token合约地址，并且可以调用合约所有的方法进行测试，如下图：

![remix invoke](./docs/remix3.png)
