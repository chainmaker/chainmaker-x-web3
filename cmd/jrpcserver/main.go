// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"

	"chainmaker.org/chainmaker/chainmaker-x-web3/backend"
	"chainmaker.org/chainmaker/chainmaker-x-web3/config"
	"chainmaker.org/chainmaker/chainmaker-x-web3/jsonrpc"
	"github.com/hashicorp/go-hclog"
)

const (
	appName = "xweb3"
)

var (
	// application's version string
	version = "v0.1.0"
	// build commit id
	build = ""
)

func serve(c *config.Config, store *backend.Store) error {
	conf := &jsonrpc.Config{
		Store:                    store,
		Addr:                     c.JSONRPCAddr,
		ChainID:                  uint64(c.ChainID),
		AccessControlAllowOrigin: c.AccessControlAllowOrigin,
		PriceLimit:               c.PriceLimit,
		BatchLengthLimit:         c.BatchLengthLimit,
		BlockRangeLimit:          c.BlockRangeLimit,
	}

	logger := hclog.New(&hclog.LoggerOptions{
		Name:  appName,
		Level: hclog.LevelFromString(c.LogLevel),
	})

	_, err := jsonrpc.NewJSONRPC(logger, conf)
	return err
}

func main() {
	vFlag := flag.Bool("v", false, "Print version and exit")
	configFilePath := flag.String("c", "./config.yml", "Config file path")
	flag.Parse()

	if *vFlag {
		fmt.Printf("Version: %s\nBuild: %s\n", version, build)
		os.Exit(0)
	}

	// Init config
	c := config.Init()

	// New chainmaker client
	cc, err := backend.NewChainmakerClient(*configFilePath)
	if err != nil {
		log.Fatalln(err)
	}
	defer cc.Stop()

	v, err := cc.GetChainMakerServerVersion()
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("Chainmaker node version:", v)

	logger := hclog.New(&hclog.LoggerOptions{
		Name:  appName,
		Level: hclog.LevelFromString(c.LogLevel),
	})
	// New store
	store := backend.NewStore(cc, logger)

	// Start serve
	err = serve(c, store)
	if err != nil {
		log.Fatalln(err)
	}

	// Wait for interrupt signal to gracefully shutdown the server.
	interrupt := make(chan os.Signal)
	signal.Notify(interrupt, os.Interrupt)
	<-interrupt
}
