COMMITHASH := $(shell git rev-parse --short HEAD)
BINARYNAME = xweb3d
GOMOD=GO111MODULE=on
GOFILES := $(wildcard *.go)
LDFLAGS=-ldflags "-w -s -X=main.build=$(COMMITHASH)"

VERSION=v3.0.0
gomod:
	go get chainmaker.org/chainmaker/common/v3@$(VERSION)
	go get chainmaker.org/chainmaker/pb-go/v3@$(VERSION)
	go get chainmaker.org/chainmaker/sdk-go/v3@$(VERSION)
	go get chainmaker.org/chainmaker/utils/v3@$(VERSION)
	go mod tidy

build:
ifeq ($(OS),Windows_NT)
	@go mod tidy && cd cmd/jrpcserver && ${GOMOD} go build $(LDFLAGS) -o ../../build/$(BINARYNAME).exe $(GOFILES)
else
	@go mod tidy && cd cmd/jrpcserver && ${GOMOD} go build $(LDFLAGS) -o ../../build/$(BINARYNAME) $(GOFILES)
endif

clean:
	@rm -rf ./build/*

test:
	@go test -v -race ./...

run:
	@cd ./cmd/jrpcserver && rm -rf sdk.log* && go run .

.PHONY: build clean
