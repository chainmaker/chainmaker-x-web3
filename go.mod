module chainmaker.org/chainmaker/chainmaker-x-web3

go 1.16

require (
	chainmaker.org/chainmaker/common/v3 v3.0.0
	chainmaker.org/chainmaker/pb-go/v3 v3.0.0
	chainmaker.org/chainmaker/sdk-go/v3 v3.0.0
	chainmaker.org/chainmaker/utils/v3 v3.0.0
	github.com/0xPolygon/polygon-edge v0.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	github.com/hashicorp/go-hclog v1.3.1
	github.com/spf13/viper v1.13.0 // indirect
	github.com/stretchr/testify v1.8.0
	github.com/umbracle/ethgo v0.1.4-0.20220722090909-c8ac32939570
	github.com/umbracle/fastrlp v0.1.0
)
