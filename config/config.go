// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package config

import (
	"net"
)

const (
	AppModeDevelop appMode = iota
	AppModeProduct
)

type appMode int

func (mode appMode) IsDevelop() bool {
	return mode == AppModeDevelop
}

func (mode appMode) IsProduct() bool {
	return mode == AppModeProduct
}

type Config struct {
	AppMode  appMode
	LogLevel string

	// JSON Rpc
	Cors                     CORS `mapstructure:"cors" json:"cors" yaml:"cors"`
	JSONRPCAddr              *net.TCPAddr
	AccessControlAllowOrigin []string
	BatchLengthLimit         uint64
	BlockRangeLimit          uint64

	ChainID    int
	PriceLimit uint64
	// ChainMakerClient chainmaker client
	ChainmakerClient ChainMakerClient `mapstructure:"chainmaker_client" json:"chainmaker_client" yaml:"chainmaker_client"`
}

var C *Config

func Init() *Config {
	//var in string
	//flag.StringVar(&in, "c", "", "config file path.")
	//flag.Parse()
	//
	//viper.SetConfigFile(in)
	//viper.SetConfigType("yaml")
	//err := viper.ReadInConfig()
	//if err != nil {
	//	panic(fmt.Errorf("panic read config file: %s\n", err))
	//}
	//viper.WatchConfig()
	//viper.OnConfigChange(func(e fsnotify.Event) {
	//	fmt.Println("config file changed:", e.Name)
	//	if err := viper.Unmarshal(&C); err != nil {
	//		fmt.Println(err)
	//	}
	//})
	//
	//if err := viper.Unmarshal(&C); err != nil {
	//	fmt.Println(err)
	//}
	//return &C

	C = &Config{
		AppMode:  AppModeProduct,
		LogLevel: "debug",
		Cors:     CORS{},
		JSONRPCAddr: &net.TCPAddr{
			IP:   net.ParseIP("0.0.0.0"),
			Port: 80,
		},
		AccessControlAllowOrigin: []string{"*"},
		BatchLengthLimit:         20,
		BlockRangeLimit:          1000,
		ChainID:                  12301,
		PriceLimit:               0,
		ChainmakerClient:         ChainMakerClient{},
	}
	return C
}
