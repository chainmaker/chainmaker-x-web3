// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package config

type ChainMakerClient struct {
	ChainAdmins []ChainMakerUser `mapstructure:"chain_admins"`
	// 链ID
	ChainId string `mapstructure:"chain_id"`
	// 同步交易结果模式下，轮训获取交易结果时的最大轮训次数
	RetryLimit int `mapstructure:"retry_limit"`
	// 同步交易结果模式下，每次轮训交易结果时的等待时间 单位：ms
	RetryInterval int `mapstructure:"retry_interval"`
	// 节点配置
	NodesConfig []ChainMakerNode `mapstructure:"nodes"`
	// 设置grpc客户端配置
	RPCClientConfig *RpcClientConfigModel `mapstructure:"rpc_client"`
	// 认证模式
	AuthType string `mapstructure:"auth_type"`
	// 区块浏览器api url
	ExplorerApiUrl string `mapstructure:"explorer_api_url"`
}

type ChainMakerUser struct {
	// 组织ID
	OrgId string `mapstructure:"org_id"`
	// 客户端用户私钥路径
	UserKeyFilePath string `mapstructure:"user_key_file_path"`
	// 客户端用户证书路径
	UserCrtFilePath string `mapstructure:"user_crt_file_path"`
	// 证书模式下：客户端用户交易签名私钥路径(若未设置，将使用user_key_file_path)
	// 公钥模式下：客户端用户交易签名的私钥路径(必须设置)
	UserSignKeyFilePath string `mapstructure:"user_sign_key_file_path"`
	// 客户端用户交易签名证书路径(若未设置，将使用user_crt_file_path)
	UserSignCrtFilePath string `mapstructure:"user_sign_crt_file_path"`
}

// ChainMakerNode 节点配置
type ChainMakerNode struct {
	// 节点地址
	NodeAddr string `mapstructure:"node_addr"`
	// 节点连接数
	ConnCnt int `mapstructure:"conn_cnt"`
	// RPC连接是否启用双向TLS认证
	EnableTLS bool `mapstructure:"enable_tls"`
	// 信任证书池路径
	TrustRootPaths []string `mapstructure:"trust_root_paths"`
	// TLS hostname
	TLSHostName string `mapstructure:"tls_host_name"`
}

type RpcClientConfigModel struct {
	MaxRecvMsgSize int `mapstructure:"max_receive_message_size"`
	MaxSendMsgSize int `mapstructure:"max_send_message_size"`
}
