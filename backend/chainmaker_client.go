// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	"errors"

	"chainmaker.org/chainmaker/pb-go/v3/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v3"
)

func NewChainmakerClient(sdkConfPath string) (*sdk.ChainClient, error) {
	return sdk.NewChainClient(
		sdk.WithConfPath(sdkConfPath),
	)
}

func CheckRespCode(resp *common.TxResponse) error {
	if resp.Code != common.TxStatusCode_SUCCESS {
		if resp.Message == "" {
			return errors.New(resp.Code.String())
		}
		return errors.New(resp.Message)
	}
	return nil
}
