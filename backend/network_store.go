// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	sdk "chainmaker.org/chainmaker/sdk-go/v3"
)

type networkStore struct {
}

func newNetworkStore(cc *sdk.ChainClient) *networkStore {
	return &networkStore{}
}

func (s *networkStore) GetPeers() int {
	return 1
}
