// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	"bytes"

	sdk "chainmaker.org/chainmaker/sdk-go/v3"
	"github.com/0xPolygon/polygon-edge/blockchain"
	"github.com/0xPolygon/polygon-edge/helper/hex"
	"github.com/0xPolygon/polygon-edge/types"
)

type filterManagerStore struct {
	cc *sdk.ChainClient
}

func newFilterManagerStore(cc *sdk.ChainClient) *filterManagerStore {
	return &filterManagerStore{cc: cc}
}

func (s *filterManagerStore) Header() *types.Header {
	block, err := s.cc.GetLastBlock(false)
	if err != nil {
		panic(err)
	}
	return convertHeader(block.Block.Header)
}

func (s *filterManagerStore) SubscribeEvents() blockchain.Subscription {
	panic("method SubscribeEvents not implemented")
}

func (s *filterManagerStore) GetReceiptsByHash(hash types.Hash) ([]*types.Receipt, error) {
	panic("method GetReceiptsByHash not implemented")
}

func (s *filterManagerStore) GetBlockByHash(hash types.Hash, full bool) (*types.Block, bool) {
	if bytes.Equal(hash[:], types.ZeroHash[:]) {
		return nil, false
	}
	block, err := s.cc.GetBlockByHash(hex.EncodeToString(hash.Bytes()), false)
	if err != nil {
		panic(err)
	}
	return convertBlock(block.Block), true
}

func (s *filterManagerStore) GetBlockByNumber(num uint64, full bool) (*types.Block, bool) {
	block, err := s.cc.GetBlockByHeight(num, full)
	if err != nil {
		panic(err)
	}
	return convertBlock(block.Block), true
}
