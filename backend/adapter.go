// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	"encoding/json"
	"math/big"
	"strconv"

	"chainmaker.org/chainmaker/pb-go/v3/common"
	"chainmaker.org/chainmaker/pb-go/v3/eth"
	"chainmaker.org/chainmaker/pb-go/v3/syscontract"
	sdk "chainmaker.org/chainmaker/sdk-go/v3"
	"chainmaker.org/chainmaker/utils/v3"
	"github.com/0xPolygon/polygon-edge/helper/hex"
	"github.com/0xPolygon/polygon-edge/state"
	"github.com/0xPolygon/polygon-edge/types"
)

func convertHeader(header *common.BlockHeader) *types.Header {
	var miner []byte
	if header.Proposer != nil {
		miner = header.Proposer.MemberInfo
	}
	return &types.Header{
		ParentHash:   types.BytesToHash(header.PreBlockHash),
		Sha3Uncles:   types.Hash{},
		Miner:        miner,
		StateRoot:    types.Hash{},
		TxRoot:       types.Hash{},
		ReceiptsRoot: types.Hash{},
		LogsBloom:    types.Bloom{},
		Difficulty:   0,
		Number:       header.BlockHeight,
		GasLimit:     0,
		GasUsed:      0,
		Timestamp:    uint64(header.BlockTimestamp),
		ExtraData:    nil,
		MixHash:      types.Hash{},
		Nonce:        types.Nonce{},
		Hash:         types.BytesToHash(header.BlockHash),
	}
}

func convertTx(tx *common.Transaction) *types.Transaction {
	if !tx.Payload.TxType.IsEthTxType() {
		return &types.Transaction{Hash: types.StringToHash(tx.Payload.TxId)}
	}
	etx, err := utils.ConvertCmTx2EthTx(tx)
	if err != nil {
		return &types.Transaction{Hash: types.StringToHash(tx.Payload.TxId)}
	}
	from, _ := etx.GetSigner()
	r, s, v := etx.GetRawSignatureValues()
	newTx := &types.Transaction{
		Nonce:    etx.GetNonce(),
		GasPrice: etx.GetGasPrice(),
		Gas:      etx.GetGas(),
		To:       nil,
		Value:    etx.GetValue(),
		Input:    etx.GetData(),
		V:        v,
		R:        r,
		S:        s,
		Hash:     types.BytesToHash(etx.GetHash().Bytes()),
		From:     types.BytesToAddress(from.Bytes()),
	}
	ethTo := etx.GetTo()
	if ethTo != nil && !ethTo.IsZero() {
		to := types.BytesToAddress(ethTo.Bytes())
		newTx.To = &to
	}
	return newTx
}

func convertBlock(block *common.Block) *types.Block {
	newBlock := &types.Block{
		Header:       convertHeader(block.Header),
		Transactions: make([]*types.Transaction, len(block.Txs)),
		Uncles:       nil,
	}
	for i, tx := range block.Txs {
		newBlock.Transactions[i] = convertTx(tx)
	}

	return newBlock
}

func getAccountInfo(cc *sdk.ChainClient, address string) *state.Account {
	result, err := cc.QuerySystemContract(syscontract.SystemContract_ETHEREUM.String(),
		syscontract.EthereumFunction_GetAccount.String(),
		[]*common.KeyValuePair{
			{
				Key:   "account",
				Value: []byte(address),
			},
		}, 0)
	if err != nil || result.Code != common.TxStatusCode_SUCCESS {
		panic(err)
	}
	userAccount := &eth.UserAccount{}
	err = json.Unmarshal(result.ContractResult.Result, &userAccount)
	if err != nil {
		panic(err)
	}
	b := big.NewInt(0)
	b.SetBytes(userAccount.Balance)
	return &state.Account{
		Nonce:    userAccount.Nonce,
		Balance:  b,
		Root:     types.Hash{},
		CodeHash: nil,
		Trie:     nil,
	}
}

func getNonce(cc *sdk.ChainClient, address string) uint64 {
	result, err := cc.QuerySystemContract(syscontract.SystemContract_ETHEREUM.String(),
		syscontract.EthereumFunction_Nonce.String(),
		[]*common.KeyValuePair{
			{
				Key:   "account",
				Value: []byte(address),
			},
		}, 0)
	if err != nil || result.Code != common.TxStatusCode_SUCCESS {
		panic(err)
	}
	nonce, _ := strconv.ParseUint(string(result.ContractResult.Result), 10, 64)
	return nonce
}

// convertReceipt 转换合约的结果为收据
// @param result
// @param contractName
// @param txId
// @return *types.Receipt
func convertReceipt(result *common.ContractResult, contractAddr types.Address, txId string) *types.Receipt {

	txHash := types.StringToHash(txId)
	status := types.ReceiptSuccess
	receipt := &types.Receipt{
		Root:              types.Hash{},
		CumulativeGasUsed: 0,
		LogsBloom:         types.Bloom{},
		Logs:              make([]*types.Log, 0),
		Status:            &status,
		GasUsed:           result.GasUsed,
		ContractAddress:   &contractAddr,
		TxHash:            txHash,
	}
	for _, event := range result.ContractEvent {
		receipt.Logs = append(receipt.Logs, convertEventLog(event))
	}
	if result.Code != 0 {
		status = types.ReceiptFailed
		receipt.Status = &status
	}
	return receipt
}

func convertEventLog(event *common.ContractEvent) *types.Log {
	log := &types.Log{
		Address: types.StringToAddress(event.ContractName),
		Topics:  make([]types.Hash, 0),
		Data:    make([]byte, 0),
	}
	if len(event.Topic) > 0 {
		log.Topics = append(log.Topics, types.StringToHash(event.Topic))
	}
	if len(event.EventData) > 0 {
		for _, d := range event.EventData {
			data, _ := hex.DecodeString(d)
			log.Data = append(log.Data, data...)
		}
	}
	return log
}
func convertTxn2CMTx(tx *types.Transaction, chainId string) *common.Transaction {
	cmTx := &common.Transaction{}
	toAddr := types.ZeroAddress
	if tx.To != nil {
		toAddr = *tx.To
	}
	cmTx.Payload = &common.Payload{
		ChainId:        chainId,
		TxType:         common.TxType_ETH_TX,
		TxId:           hex.EncodeToString(tx.Hash[:]),
		Timestamp:      0,
		ExpirationTime: 0,
		ContractName:   toAddr.String(),
		Method:         "",
		Parameters: []*common.KeyValuePair{
			{
				Key:   common.EthTxParameterKey_VALUE.String(),
				Value: tx.Value.Bytes(),
			}, {
				Key:   common.EthTxParameterKey_DATA.String(),
				Value: tx.Input,
			}, {
				Key:   common.EthTxParameterKey_FROM.String(),
				Value: tx.From.Bytes(),
			},
		},
		Sequence: tx.Nonce,
		Limit: &common.Limit{
			GasLimit: tx.Gas,
			GasPrice: tx.GasPrice.Uint64(),
		},
	}
	return cmTx
}
