// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"math/big"
	"strings"

	"chainmaker.org/chainmaker/chainmaker-x-web3/config"
	"chainmaker.org/chainmaker/common/v3/ethbase"
	commonPb "chainmaker.org/chainmaker/pb-go/v3/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v3"
	"chainmaker.org/chainmaker/utils/v3"
	"github.com/0xPolygon/polygon-edge/chain"
	"github.com/0xPolygon/polygon-edge/helper/progress"
	"github.com/0xPolygon/polygon-edge/state"
	"github.com/0xPolygon/polygon-edge/state/runtime"
	"github.com/0xPolygon/polygon-edge/types"
	"github.com/hashicorp/go-hclog"
)

type ethStore struct {
	*ethBlockchainStore
	*ethStateStore
	*ethTxPoolStore
}

func newEthStore(cc *sdk.ChainClient, logger hclog.Logger) *ethStore {
	return &ethStore{
		ethBlockchainStore: newEthBlockchainStore(cc, logger),
		ethStateStore:      newEthStateStore(cc),
		ethTxPoolStore:     newEthTxPoolStore(cc),
	}
}

type ethBlockchainStore struct {
	cc     *sdk.ChainClient
	logger hclog.Logger
}

func newEthBlockchainStore(cc *sdk.ChainClient, logger hclog.Logger) *ethBlockchainStore {
	return &ethBlockchainStore{
		cc:     cc,
		logger: logger,
	}
}

func (s *ethBlockchainStore) Header() *types.Header {
	block, err := s.cc.GetLastBlock(false)
	if err != nil {
		panic(err)
	}
	return convertHeader(block.Block.Header)
}

func (s *ethBlockchainStore) GetHeaderByNumber(block uint64) (*types.Header, bool) {
	blk, err := s.cc.GetBlockByHeight(block, false)
	if err != nil || blk.Block == nil {
		return nil, false
	}
	return convertHeader(blk.Block.Header), true
}

func (s *ethBlockchainStore) GetBlockByHash(hash types.Hash, full bool) (*types.Block, bool) {
	if bytes.Equal(hash[:], types.ZeroHash[:]) {
		return nil, false
	}
	blockHashStr := hex.EncodeToString(hash[:])
	blk, err := s.cc.GetBlockByHash(blockHashStr, full)
	if err != nil {
		panic(err)
	}
	return convertBlock(blk.Block), true
}

func (s *ethBlockchainStore) GetBlockByNumber(num uint64, full bool) (*types.Block, bool) {
	block, err := s.cc.GetBlockByHeight(num, full)
	if err != nil {
		panic(err)
	}
	return convertBlock(block.Block), true
}

func (s *ethBlockchainStore) GetTxByHash(txnHash types.Hash) (tx *types.Transaction, blockNumber uint64,
	blockHash types.Hash, txIndex int, err error) {
	if bytes.Equal(txnHash[:], types.ZeroHash[:]) {
		return nil, 0, types.ZeroHash, 0, errors.New("txhash is zero")
	}

	txId := hex.EncodeToString(txnHash.Bytes())
	cmTxInfo, err := s.cc.GetTxByTxId(txId)
	if err != nil {
		return
	}

	cmTx := cmTxInfo.Transaction
	tx = convertTx(cmTx)
	//ethTx, err := utils.ConvertCmTx2EthTx(cmTx)
	//if err != nil {
	//	return nil, 0, types.ZeroHash, 0, err
	//}
	//
	//from, _ := ethTx.GetSigner()
	//r, s, v := ethTx.GetRawSignatureValues()
	//tx = &types.Transaction{
	//	Nonce:    ethTx.GetNonce(),
	//	GasPrice: ethTx.GetGasPrice(),
	//	Gas:      ethTx.GetGas(),
	//	To:       nil,
	//	Value:    ethTx.GetValue(),
	//	Input:    ethTx.GetData(),
	//	V:        v,
	//	R:        r,
	//	S:        s,
	//	Hash:     types.BytesToHash(ethTx.Hash[:]),
	//	From:     types.BytesToAddress(from.Bytes()),
	//}
	//ethTo := ethTx.GetTo()
	//if ethTo != nil && !ethTo.IsZero() {
	//	to := types.BytesToAddress(ethTo.Bytes())
	//	tx.To = &to
	//
	//}

	blockNumber = cmTxInfo.BlockHeight
	blockHash = types.BytesToHash(cmTxInfo.BlockHash)
	if cmTxInfo.BlockHash == nil {
		header, _ := s.cc.GetBlockHeaderByHeight(blockNumber)
		blockHash = types.BytesToHash(header.BlockHash)
	}
	txIndex = int(cmTxInfo.TxIndex)
	return
}

//func (s *ethBlockchainStore) GetReceiptsByHash(blkHash types.Hash) ([]*types.Receipt, error) {
//	blk, err := s.cc.GetBlockByHash(blkHash.String(), false)
//	if err != nil {
//		return nil, err
//	}
//	result := make([]*types.Receipt, len(blk.Block.Txs))
//	for i, tx := range blk.Block.Txs {
//		result[i] = convertReceipt(tx.Result.ContractResult, tx.Payload.ContractName, tx.Payload.TxId)
//	}
//	return result, nil
//}

func (s *ethBlockchainStore) GetReceiptByTxHash(txHash types.Hash) (tx *types.Receipt, blockNumber uint64,
	blockHash types.Hash, txIndex int, err error) {
	if bytes.Equal(txHash[:], types.ZeroHash[:]) {
		return nil, 0, types.ZeroHash, 0, errors.New("txhash is zero")
	}
	txId := hex.EncodeToString(txHash.Bytes())
	cmTxInfo, err := s.cc.GetTxByTxId(txId)
	if err != nil {
		return nil, 0, types.ZeroHash, 0, err
	}
	contractAddr := types.StringToAddress(cmTxInfo.Transaction.Payload.ContractName)
	//如果是合约安装，那么合约地址应该从Result中的Contract对象中解析
	s.logger.Debug("GetReceiptByTxHash", "contractAddr", contractAddr.String())
	if contractAddr == types.ZeroAddress {
		contract := &commonPb.Contract{}
		contract.Unmarshal(cmTxInfo.Transaction.Result.ContractResult.Result)
		s.logger.Debug("get an install contract tx receipt", "contract name", contract.Name)
		contractAddr = types.StringToAddress(contract.Name)
	}
	tx = convertReceipt(cmTxInfo.Transaction.Result.ContractResult, contractAddr, cmTxInfo.Transaction.Payload.TxId)
	blockNumber = cmTxInfo.BlockHeight
	blockHash = types.BytesToHash(cmTxInfo.BlockHash)
	txIndex = int(cmTxInfo.TxIndex)
	return
}

var OneGWei = big.NewInt(1000000000)

func (s *ethBlockchainStore) GetAvgGasPrice() *big.Int {
	return OneGWei
}

// ApplyTxn 查询EVM合约或者是预估Gas用
// @param header
// @param txn
// @return *runtime.ExecutionResult
// @return error
func (s *ethBlockchainStore) ApplyTxn(header *types.Header, tx *types.Transaction) (*runtime.ExecutionResult, error) {
	if tx.To == nil {
		//install contract
		payload, _ := utils.GenerateInstallContractPayload(tx.Hash.String(), "v1",
			commonPb.RuntimeType_EVM, []byte(tx.Input), nil)
		payload.ChainId = s.cc.ConfigModel.ChainClientConfig.ChainId
		payload.TxId = hex.EncodeToString(tx.Hash[:])
		gas, err := s.cc.EstimateGas(payload)
		if err != nil {
			return nil, err
		}
		return &runtime.ExecutionResult{
			ReturnValue: nil,
			GasLeft:     0,
			GasUsed:     gas,
			Err:         nil,
		}, nil
	}
	//转账
	if len(tx.Input) == 0 {
		return &runtime.ExecutionResult{
			ReturnValue: nil,
			GasLeft:     0,
			GasUsed:     21000,
			Err:         nil,
		}, nil
	}
	contractName := strings.ToLower(tx.To.String())
	method := ""
	if len(tx.Input) > 4 {
		method = hex.EncodeToString(tx.Input[0:4])
	}
	result, err := s.cc.QueryContract(contractName, method, []*commonPb.KeyValuePair{&commonPb.KeyValuePair{
		Key:   "data",
		Value: []byte(hex.EncodeToString(tx.Input)),
	}}, 0)
	if err != nil {
		fmt.Printf("QueryContract fail, err:%s", err)
		return &runtime.ExecutionResult{
			ReturnValue: nil,
			GasLeft:     0,
			GasUsed:     21000,
			Err:         err,
		}, err
	}

	return &runtime.ExecutionResult{
		ReturnValue: result.ContractResult.Result,
		GasLeft:     0,
		GasUsed:     result.ContractResult.GasUsed,
		Err:         nil,
	}, nil
}

func (s *ethBlockchainStore) GetSyncProgression() *progress.Progression {
	panic("method GetSyncProgression not implemented")
}

func (s *ethBlockchainStore) EstimateGas(txn *types.Transaction) (uint64, error) {
	if txn == nil {
		return 0, errors.New("tx is nil")
	}

	if config.C.AppMode.IsDevelop() {
		return 200000, nil
	}
	//转账时固定金额
	//if len(txn.Input) < 4 {
	//	return 21000, nil
	//}
	cmTx := convertTxn2CMTx(txn, s.cc.ConfigModel.ChainClientConfig.ChainId)
	return s.cc.EstimateGas(cmTx.Payload)
}

////////////////////////////////////////////////////////////////////////////////////////////////////

type ethStateStore struct {
	cc *sdk.ChainClient
}

func newEthStateStore(cc *sdk.ChainClient) *ethStateStore {
	return &ethStateStore{cc}
}

func (s *ethStateStore) GetAccount(root types.Hash, addr types.Address) (*state.Account, error) {
	return getAccountInfo(s.cc, addr.String()), nil
}

func (s *ethStateStore) GetStorage(root types.Hash, addr types.Address, slot types.Hash) ([]byte, error) {
	return s.cc.GetStateByKey(addr.String(), slot.String())
}

func (s *ethStateStore) GetForksInTime(blockNumber uint64) chain.ForksInTime {
	return chain.ForksInTime{
		Homestead:      true,
		Byzantium:      true,
		Constantinople: true,
		Petersburg:     true,
		Istanbul:       true,
		EIP150:         true,
		EIP158:         true,
		EIP155:         true,
	}
}

func (s *ethStateStore) GetCode(addr types.Address) ([]byte, error) {
	//fmt.Println("call GetCode, hash:", hash.String())
	return s.cc.GetContractByteCode(addr.String())
}

////////////////////////////////////////////////////////////////////////////////////////////////////

type ethTxPoolStore struct {
	cc *sdk.ChainClient
}

func newEthTxPoolStore(cc *sdk.ChainClient) *ethTxPoolStore {
	return &ethTxPoolStore{
		cc: cc,
	}
}

func (s *ethTxPoolStore) GetNonce(addr types.Address) uint64 {
	return getNonce(s.cc, addr.String())
}

func (s *ethTxPoolStore) AddTx(tx *types.Transaction) error {
	if tx == nil {
		return errors.New("tx is nil")
	}
	txBz := tx.MarshalRLP()

	req := &commonPb.TxRequest{Payload: &commonPb.Payload{TxType: commonPb.TxType_ETH_TX}}
	req.Payload.Parameters = []*commonPb.KeyValuePair{
		{
			Key:   "rawtx",
			Value: txBz,
		},
	}
	req.Payload.TxId = hex.EncodeToString(ethbase.Keccak256(txBz))

	resp, err := s.cc.SendTxRequest(req, 10, false)
	if err != nil {
		return err
	}

	return CheckRespCode(resp)
}

func (s *ethTxPoolStore) GetPendingTx(txHash types.Hash) (*types.Transaction, bool) {
	txId := hex.EncodeToString(txHash[:])
	txs, _, err := s.cc.GetTxsInPoolByTxIds([]string{txId})
	if err != nil || len(txs) == 0 {
		return nil, false
	}
	return convertTx(txs[0]), true
}
