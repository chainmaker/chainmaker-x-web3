// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	sdk "chainmaker.org/chainmaker/sdk-go/v3"
	"github.com/hashicorp/go-hclog"
)

type Store struct {
	*ethStore
	*networkStore
	*txPoolStore
	*filterManagerStore
}

func NewStore(cc *sdk.ChainClient, logger hclog.Logger) *Store {
	return &Store{
		ethStore:           newEthStore(cc, logger.Named("ethStore")),
		networkStore:       newNetworkStore(cc),
		txPoolStore:        newTxPoolStore(cc),
		filterManagerStore: newFilterManagerStore(cc),
	}
}
