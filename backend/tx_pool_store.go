// Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
// SPDX-License-Identifier: Apache-2.0

package backend

import (
	sdk "chainmaker.org/chainmaker/sdk-go/v3"
	"github.com/0xPolygon/polygon-edge/types"
)

type txPoolStore struct {
}

func newTxPoolStore(cc *sdk.ChainClient) *txPoolStore {
	return &txPoolStore{}
}

func (s *txPoolStore) GetTxs(inclQueued bool) (map[types.Address][]*types.Transaction, map[types.Address][]*types.Transaction) {
	panic("method GetTxs not implemented")
}

func (s *txPoolStore) GetCapacity() (uint64, uint64) {
	panic("method GetCapacity not implemented")
}
